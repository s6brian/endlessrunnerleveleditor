using System.Xml.Serialization;  
using System.IO;
using UnityEngine;
using System.Xml;
using System.Text;

public class XmlHelper
{
	public static object Load( string p_filename, System.Type p_type )
	{
		XmlSerializer serializer = new XmlSerializer( p_type );
		Stream stream;

		if(p_filename.Contains(":\\"))
		{
			stream = new FileStream( p_filename, FileMode.Open );
		}
		else
		{
			TextAsset textAsset = (TextAsset)Resources.Load( "Xml/" + p_filename, typeof(TextAsset));
			stream = new MemoryStream( textAsset.bytes );
		}

		object container = serializer.Deserialize( stream );
		stream.Close();
		return container;
	}
	
	public static void Save( string p_filename, System.Type p_type, object p_instance )
	{
		XmlSerializer serializer = new XmlSerializer( p_type );
		string filename = p_filename.Contains(":\\") ? p_filename : GetPath( p_filename );
		Encoding encoding = Encoding.GetEncoding( "UTF-8" );
	
		using( StreamWriter stream = new StreamWriter( filename, false, encoding )) {
			serializer.Serialize ( stream, p_instance );
			stream.Close();
		}
	}

	public static string GetPath( string p_filename )
	{
		return Application.dataPath + "/Resources/Xml/" + p_filename + ".xml";
	}

	public static XmlElement CreateElement(XmlDocument xmlObject, string elementName, string innerValue, string[] attributeList, string[] attributeValues)
	{
		XmlElement element = xmlObject.CreateElement(elementName);
		int i = 0;
		// Process inner value
		if(innerValue != null){
			element.InnerText = innerValue;
		}
		// Process attributes
		if(attributeList != null){
			foreach(string attribute in attributeList){
				element.SetAttribute(attribute, attributeValues[i]);
				i++;
			}
		}
		return element;
	}
} 
