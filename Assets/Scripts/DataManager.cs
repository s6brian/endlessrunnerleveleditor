﻿using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
	private LevelData m_levelData;
	private List<Level> m_listEasyLevels;
	private List<Level> m_listMediumLevels;
	private List<Level> m_listHardLevels;

	public LevelData Data { set{ m_levelData = value; }
							get{ return m_levelData; }}
	public List<Level> EasyLevels   {get{ return m_listEasyLevels;   }}
	public List<Level> MediumLevels {get{ return m_listMediumLevels; }}
	public List<Level> HardLevels   {get{ return m_listHardLevels;   }}

	protected void Awake() 
	{
		m_levelData        = new LevelData();
		m_listEasyLevels   = new List<Level>();
		m_listMediumLevels = new List<Level>();
		m_listHardLevels   = new List<Level>();

		// TestData();
	}

	private void TestData()
	{
		for(int idx = 0; idx < 3; ++idx)
		{
			Level l = new Level();

			for(int jdx = 0; jdx < 2; ++jdx)
			{
				Obstacle o = new Obstacle();
				o.row = jdx * 2;
				o.map = Random.Range(0, 511);
				l.listObstacles.Add(o);
			}

			m_listMediumLevels.Add(l);
		}
	}

	public void ResetData()
	{
		m_levelData.listEasyLevels   = null;
		m_levelData.listMediumLevels = null;
		m_levelData.listHardLevels   = null;
	}

	public void UpdateData()
	{
		if(m_listEasyLevels.Count > 0)
		{
			m_levelData.listEasyLevels = m_listEasyLevels;
		}

		if(m_listMediumLevels.Count > 0)
		{
			m_levelData.listMediumLevels = m_listMediumLevels;
		}

		if(m_listHardLevels.Count > 0)
		{
			m_levelData.listHardLevels = m_listHardLevels;
		}
	}
}
