﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System.Collections.Generic;

public class ObstacleFactoryPanel : MonoBehaviour
{
	[SerializeField] private DataManager m_dataManager;
	[SerializeField] private Dropdown    m_dropdownDifficulty;
	[SerializeField] private Dropdown    m_dropdownLevels;
	[SerializeField] private Dropdown    m_dropdownRow;
	[SerializeField] private Transform   m_obstacleGrid;

	private int m_currentDifficulty;
	private int m_currentLevel;

	private List<Level>[] m_arrayOfLevelsList;
	private List<Level> m_currentLevelsList;
	private List<Obstacle> m_currentObstaclesList;
	private List<int> m_availableRowsList;
	private Toggle[] m_obstacleMap;

	private GameObject m_gameobject;
	private bool m_bIsNew;

	protected void Awake() 
	{
		Assert.IsNotNull(m_dataManager);
		Assert.IsNotNull(m_dropdownDifficulty);
		Assert.IsNotNull(m_dropdownLevels);
		Assert.IsNotNull(m_dropdownRow);
		Assert.IsNotNull(m_obstacleGrid);
		
		m_gameobject = this.gameObject;
		m_gameobject.SetActive(false);

		m_arrayOfLevelsList = new List<Level>[]
		{
			m_dataManager.EasyLevels,
			m_dataManager.MediumLevels,
			m_dataManager.HardLevels
		};

		m_obstacleMap = m_obstacleGrid.GetComponentsInChildren<Toggle>();
		m_currentDifficulty = -1;
		m_currentLevel = -1;
	}

	private void UpdateAvailableRows()
	{
		m_availableRowsList = new List<int>();
		int maxRow = m_currentLevelsList[m_currentLevel].maxrow;
		
		for(int idx = 0; idx < maxRow; ++idx)
		{
			m_availableRowsList.Add(idx);
		}

		foreach(Obstacle obstacle in m_currentObstaclesList)
		{
			m_availableRowsList.Remove(obstacle.row);
		}
	}

	public void Open(bool p_isNew) 
	{
		m_gameobject.SetActive(true);
		m_bIsNew = p_isNew;

		OnDifficultyChange(m_dropdownDifficulty.value);
	}

	public void Close()
	{
		m_gameobject.SetActive(false);
	}

	public void Save()
	{
		Obstacle obstacle = new Obstacle();
		// obstacle.row = m_availableRowsList

		// check change in difficulty. update if neccessary
		// set row
		// set map

		// obstacle map indeces
		/* [ 2, 1, 0 ]
		 * [ 5, 4, 3 ]
		 * [ 8, 7, 6 ]
		 * =============
		 * [ 8, 7, 6 ] [ 5, 4, 3 ] [ 2, 1, 0 ]
		 */
		 for(int idx = 0; idx < 9; ++idx)
		 {
			 obstacle.map |= m_obstacleMap[idx].isOn ? 1 << idx : 0;
		 }

		 m_currentObstaclesList.Add(obstacle);
	}

	public void NewLevel()
	{
		// List<Level> listLevel = m_arrayOfLevelsList[m_currentDifficulty];
		// int count = listLevel.Count;

		// listLevel.Add(new Level());

		int count = m_currentLevelsList.Count;
		Level newLevel = new Level();
		m_currentLevelsList.Add(newLevel);
		m_currentObstaclesList = newLevel.listObstacles;

		m_dropdownLevels.options.Add(new Dropdown.OptionData("Level " + count));
		m_dropdownLevels.value = count;
		m_dropdownLevels.Hide();
		m_dropdownLevels.RefreshShownValue();
	}

	public void OnDifficultyChange(int p_index)
	{
		if(m_currentDifficulty == p_index){ return; }

		m_currentDifficulty    = p_index;
		m_currentLevelsList    = m_arrayOfLevelsList[m_currentDifficulty];
		m_currentLevel         = Mathf.Clamp(m_currentLevel, 0, m_currentLevelsList.Count - 1);
		m_currentObstaclesList = m_currentLevelsList[m_currentLevel].listObstacles;

		RefreshLevels();
		// RefreshRows();

		// OnLevelChange(m_currentLevel);
		
		// List<Level> listLevel = m_difficultyArray[p_index];
		// int count = listLevel.Count;

		// m_dropdownLevels.ClearOptions();
		// m_dropdownRow.ClearOptions();

		// for(int idx = 0; idx < count; ++idx)
		// {
		// 	m_dropdownLevels.options.Add(new Dropdown.OptionData("Level " + idx));
		// }

		// if(count <= 0){ return; }
		// OnLevelChange(Mathf.Clamp(m_dropdownLevels.value, 0, m_dropdownLevels.options.Count));
		// // m_dropdownLevels.value = Mathf.Clamp(m_dropdownLevels.value, 0, m_dropdownLevels.options.Count);
		// // m_dropdownLevels.RefreshShownValue();

		// foreach(Obstacle obstacle in listLevel[m_dropdownLevels.value].listObstacles)
		// {
		// 	m_dropdownRow.options.Add(new Dropdown.OptionData("On Row " + obstacle.row));
		// }

		// m_dropdownRow.value = Mathf.Clamp(m_dropdownRow.value, 0, m_dropdownRow.options.Count);
		// m_dropdownRow.RefreshShownValue();
	}

	public void OnLevelChange(int p_index)
	{
		if(m_currentLevel == p_index){ return; }
		m_currentLevel = p_index;
		m_currentObstaclesList = m_currentLevelsList[m_currentLevel].listObstacles;
		RefreshLevels();

		// m_dropdownLevels.value = p_index;
		// m_dropdownLevels.RefreshShownValue();

		// List<Level> listLevel = m_arrayOfLevelsList[m_currentDifficulty];
		// List<Obstacle> obstacles = listLevel[m_currentLevel].listObstacles;
		// int obstacleCount = obstacles.Count;

		// if(obstacleCount <= 0){ return; }
	}

	public void RefreshLevels()
	{
		int levelsCount = m_currentLevelsList.Count;
		m_dropdownLevels.ClearOptions();

		if( levelsCount <= 0 )
		{
			m_currentLevel = -1;
			m_dropdownLevels.value = 0;
			m_dropdownLevels.RefreshShownValue();

			return;
		}

		for(int idx = 0; idx < levelsCount; ++idx)
		{
			m_dropdownLevels.options.Add(new Dropdown.OptionData("Level " + idx));
		}

		m_dropdownLevels.value = m_currentLevel;
		m_dropdownLevels.RefreshShownValue();
	}

	// not needed. remove later.
	public void RefreshRows()
	{
		int rowsCount = m_currentObstaclesList.Count;
		m_dropdownRow.ClearOptions();

		if( rowsCount <= 0 )
		{
			m_dropdownRow.value = 0;
			m_dropdownRow.RefreshShownValue();

			return;
		}

		for(int idx = 0; idx < rowsCount; ++idx)
		{
			m_dropdownRow.options.Add(new Dropdown.OptionData("On Row " + m_currentObstaclesList[idx].row));
		}

		// List<Level> listLevel = m_difficultyArray[m_currentDifficulty];
		// if( listLevel.Count <= 0 ){ return; }
		// Level lvl = listLevel[m_dropdownLevels.value];

		// m_dropdownRow.ClearOptions();

		// foreach(Obstacle obstacle in listLevel[m_dropdownLevels.value].listObstacles)
		// {
		// 	m_dropdownRow.options.Add(new Dropdown.OptionData("On Row " + obstacle.row));
		// }

		m_dropdownRow.value = Mathf.Clamp(m_dropdownRow.value, 0, rowsCount-1);
		m_dropdownRow.RefreshShownValue();
	}
}
