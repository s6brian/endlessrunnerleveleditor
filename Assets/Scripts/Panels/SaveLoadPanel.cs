﻿using System.Runtime.InteropServices;
using System.Windows.Forms;
using UnityEngine;
using UnityEngine.Assertions;

public class SaveLoadPanel : MonoBehaviour
{
	[SerializeField] private DataManager m_dataManager;

	protected void Awake()
	{
		Assert.IsNotNull(m_dataManager);
	}

	public void Save()
	{
		m_dataManager.ResetData();
		m_dataManager.UpdateData();

		SaveFileDialog sfd = new SaveFileDialog();
		sfd.Filter = "XML|*.xml";
		// sfd.InitialDirectory = "c:\\";
		// sfd.RestoreDirectory = true;

		if( sfd.ShowDialog() == DialogResult.OK)
		{
			XmlHelper.Save(sfd.FileName, typeof(LevelData), m_dataManager.Data);
		}
	}

	public void Load()
	{
		OpenFileDialog ofd = new OpenFileDialog();
		ofd.Filter = "XML|*.xml";
		// ofd.InitialDirectory = "c:\\";
		// ofd.RestoreDirectory = true;
		
		if( ofd.ShowDialog() == DialogResult.OK)
		{
			m_dataManager.Data = (LevelData) XmlHelper.Load(ofd.FileName, typeof(LevelData));
		}
	}

	public void NewFile()
	{
		m_dataManager.Data = new LevelData();
		SaveFileDialog sfd = new SaveFileDialog();
		sfd.Filter = "XML|*.xml";
		sfd.Title = "Create New Level Data";
		// sfd.InitialDirectory = "c:\\";
		// sfd.RestoreDirectory = true;

		if( sfd.ShowDialog() == DialogResult.OK)
		{
			XmlHelper.Save(sfd.FileName, typeof(LevelData), m_dataManager.Data);
		}
	}

	public void Delete()
	{
		
	}

	[DllImport("user32.dll")]
	private static extern void SaveFileDialog();

	[DllImport("user32.dll")]
	private static extern void OpenFileDialog();
}
