﻿using UnityEngine;

public class ControlPanel : MonoBehaviour
{
	[SerializeField] private GameObject m_objEasyLevels;
	[SerializeField] private GameObject m_objMediumLevels;
	[SerializeField] private GameObject m_objHardLevels;

	[SpaceAttribute(10f)]
	[SerializeField] private GameObject m_objCreateObstacle;

	public void CreateObstacle()
	{

	}
}

public enum LevelDifficulty
{
	Easy,
	Medium,
	Hard
}