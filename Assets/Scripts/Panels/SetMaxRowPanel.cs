﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class SetMaxRowPanel : MonoBehaviour
{
	[SerializeField] private DataManager m_dataManager;
	[SerializeField] private InputField m_inputfield;

	private List<Level>[] m_difficultyArray;
	private int m_difficulty;
	private int m_level;

	protected void Awake() 
	{
		Assert.IsNotNull(m_dataManager);

		m_difficultyArray = new List<Level>[]
		{
			m_dataManager.EasyLevels,
			m_dataManager.MediumLevels,
			m_dataManager.HardLevels
		};

		this.gameObject.SetActive(false);
	}

	public void Open( int p_dificulty, int p_level )
	{
		m_difficulty = p_dificulty;
		m_level = p_level;
	}

	public void SetNewMaxRow()
	{
		List<Level> listLevel = m_difficultyArray[m_difficulty];
		if( listLevel.Count <= 0 ){ return; }
		Level lvl = listLevel[m_level];
		lvl.maxrow = int.Parse(m_inputfield.text);
	} 
}
