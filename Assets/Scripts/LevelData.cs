﻿using System.Xml.Serialization;
using System.Collections.Generic;

[XmlRoot("leveldata")]
public class LevelData
{
	[XmlArray("easy"  , IsNullable = false)] public List<Level> listEasyLevels;
	[XmlArray("medium", IsNullable = false)] public List<Level> listMediumLevels;
	[XmlArray("hard"  , IsNullable = false)] public List<Level> listHardLevels;
}

public class Level
{
	// [XmlAttribute] public string name = "level";
	[XmlAttribute] public int maxrow = 3;
	[XmlArray("obstacles")] public List<Obstacle> listObstacles = new List<Obstacle>();
}

public class Obstacle
{
	[XmlAttribute] public int row = 0;
	[XmlAttribute] public int map = 0;
}
